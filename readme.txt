=== Membership Simplified ===
Contributors: OfficeAutopilot
Tags: Membership Plugin, Membership, OfficeAutopilot, Moonray, Ontraport
Requires at least: 3.0
Tested up to: 3.4.2
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Membership Simplified allows you to generate membership lessons with templated content to create a unified look and feel throughout your courses.


== Description ==

Membership Simplified allows you to generate membership lessons with templated content to create a unified look and feel throughout your courses. It also provides the inner workings such as navigation options, a login widget, and editor buttons to use when protecting any post or page content. It sits on top of PilotPress, thus allowing you to use videos from the video manager, downloadable files from the file manager, and much more. Here is a list of the features:

* Disable the plugin on the front end while you work on creating and adding your content. Simply turn everything on with a click of a button as soon as you are ready to go!
* Use the styling that comes with your current theme, use our default styling, or customize every single bit till your heart is content. You have the option to control the colors, font type, font size, link colors, border colors and size, and much more!
* Apply your custom styling to all of your membership content pages or just to individual ones. Its up to you.
* Easily integrate social media sharing and even facebook comments with your membership content. When your users share the content on the page, it will show a snippet of your content on their social media profiles. Then, when one of their friends click the link, they will be prompted to join before they get access! (You must have PilotPress installed and configured properly for this to happen)
* Add lessons / modules to a specific 'Membership Content' area in the backend so that you can keep your membership content organized. Then, simply drag and drop your lessons into the order you would like them to appear in. Want to add categories for your content? Great! You can do that too.
* Choose between two overview layouts to display your content differently. (See Overview Templates below.)
* Choose between two membership content layouts to display your membership content differently. (See Text / Media Templates below)
* Easily add info about your lesson as well as pertinent download materials into your membership content pages. You can either upload your downloads to your site or pull them directly from your OfficeAutopilot file manager.
* Easily add a main media item to your membership content pages. You can either upload your image to your site, pull an image from your OfficeAutopilot Image library, add the url of a video you would like to stream (supports Youtube, Vimeo, or other self hosted video links), or pull a video from your OfficeAutopilot Video Manager (The preferred way to make you look more professional.)
* Add custom HTML code to your membership content pages so that you can easily add banner images, links, or anything else you might want!
* OAP Membership Plugin also comes with a sidebar widget for your membership content menu items, so that you can allow your members to easily get to your content even when they arent on a content page!


Please visit http://membership.officeautopilot.com for more info.

(This plugin requires PilotPress (http://wordpress.org/extend/plugins/pilotpress/) and an active OfficeAutopilot account to work.)


== Installation ==

1. Unzip the downloaded oapmembership zip file

2. Upload the `oapmembership` folder and its contents into the `wp-content/plugins/` directory of your WordPress installation

3. Activate OAP Membership from Plugins page

4. Configure the OAP Membership settings from the OAP Membership option in the Settings menu.


== Frequently Asked Questions ==
For more help, please visit the help section under 'Membership Content' on the WordPress side menu or visit http://membership.officeautopilot.com.



== Changelog ==

= Beta 1.30 =
* Fixed a bug where users were unable to search pages / posts.
* Fixed an issue that was causing a 'headers already sent' warning message
* Add a fade in / out effect between videos
* Fixed an error when downloading manually uploaded downloadable files
* Fixed an error with the scrollbars not working with the scrollwheel





= Beta 1.29 =
* Added theme support for Thesis
* Minor change to style.css where we changed #content li from 18px!important to 1.5!important.





= Beta 1.28 =

* Completely recoded the video player to fix the sizing bugs that were occuring on OAP hosted videos
* Added an option to add video thumbnails for OAP hosted videos.
* Fixed a bug where the lesson menu items in the sidebar still appeared when the global post setting was disabled.
* Added an option in the Settings menu > Menu Items (Sidebar & Overview section) titled Sidebar Menu Item - Bottom Spacing. This allows you to specify the amount of spacing between menu items.
* Added an option in the Settings menu > Menu Items (Sidebar & Overview section) titled Overview Item - Bottom Spacing. This allows you to specify the amount of spacing between overview items.
* Add the ability to add comments to lesson pages.
* Removed OAP jquery and tinyscrollbar scripts as well as oapmembership.css from loading multiple times within the body of the page and used the wp_enqueue_scripts / styles function to hook them into the header.
* Fixed a bug where the WordPress image uploader would not work because it was conflicting with the image uploaders included with this plugin.
* Fixed a bug where the lesson title and lesson number positioning was not working for text templates.
* Fixed a styling issue on the bottom spacing of squared overview sections





= Beta 1.27 =
* Changed the name from OAP Membership Content to Membership Simplified
* Fixes the issue where the plugin fails activation if the plugin 'duplicate posts' or a built in theme version of 'duplicate-posts' is installed.
* Adds additional shortcode buttons to the visual editor to make it easy to add the PilotPress 'Show If' tags in your content.
* Fixes the issue where the plugin fails activation if OptimizePress is the current theme.
* Changed the custom post type url 'oaplesson' in the lesson url's to 'm'.
* Fixes the 720 by 420 left alignment issue on some themes.
* Fixes the video size issue on the shared template.
* Added Support for Vimeo https (secure) videos
* Added Support for Youtube youtu.be videos
* Added Support for Youtube https videos
* Added Support for Optimize Press
* Removed Support for Embedding Video code (Will come back out in a later release.)
* Updated the styling for the two widgets
* Made a fix to functions.php where oap_posts was defined instead of wp_posts
* Fixed the text template where the Info Box was getting displayed even when it was turned off when using the text template.
* Fixed a bug where upon adding apostrophes, a slash was getting added next to them.
* Changed the name of the shortcode functions to prevent conflicts between other tinymce shortcode button functions.
* Made changes to style.css to fix issues where sub menu items were not displaying.
* Fixed global overrides for video positioning
* Fixed the ability to add and remove menu items
* Fixes the randomly added slashes that were being added to the names of download and video items
* Made a spelling fix in the 'Settings' screen. Changed Infox to Info Box.
* Fixed a bug with the image uploader for the lesson icons





= Beta 1.26c =
* Fixes the strange spacing issue below videos and above the main text and sidebar.
* Increases the amount of videos that display by default on the membership overview pages from 6 to 20 at a time.
* Fixes the issue where the membership plugin installs jquery without first checking to see if jquery already exists.
* Adds support for 'bullet points' on all list items within the main lesson text area.
* Fixes the issue where the membership lesson icons were not displaying the proper text field to add images in the backend.




= Beta 1.26b =
* Includes a fix for Installation Problems with Optimize Press


= Beta 1.26 =
* Initial public release of beta. Please visit membership.officeautopilot.com for more info.





= Beta 1.24 =
Major Update will list all new features since 1.05 in the next release.





= Beta 1.12 =
* 4-4-12 - Bug Fixes and New Feature

* Merging code in from Pin

* Fixing layout issues

* Fixing Bugs





= Beta 1.09 =
* 3-27-12 - Bug Fixes and New Additions

* New Help Menu

* All items are now under the membership content section

* More...





= Beta 1.08 =
* 3-26-12 - Bug Fixes and New Additions




= Beta 1.07p =
* -20-12 - Bug Fixes

* ilotPress and other shortcodes now work properly. Feature patched by Pin.
